# SearchShops.com price comparison #

This is API searching for products.

Based on [SearchShops.com](https://SearchShops.com/) price comparison website.

* This API allows to search products using REST API.
* Please obtain API key before using.

# SearchShops.com search for products api #
